package hw1;

import java.util.Map;

public final class ProductBuilder {
    private int height;
    private int length;
    private String color;
    private Product.ProductType productType;
    private int leftInStock;
    private boolean discountable;
    private double defaultPrice;
    private Map<String, ?> descriptionAttributes;

    private ProductBuilder() {
    }

    public static ProductBuilder aProduct() {
        return new ProductBuilder();
    }

    public ProductBuilder withHeight(int height) {
        this.height = height;
        return this;
    }

    public ProductBuilder withLength(int length) {
        this.length = length;
        return this;
    }

    public ProductBuilder withColor(String color) {
        this.color = color;
        return this;
    }

    public ProductBuilder withProductType(Product.ProductType productType) {
        this.productType = productType;
        return this;
    }

    public ProductBuilder withLeftInStock(int leftInStock) {
        this.leftInStock = leftInStock;
        return this;
    }

    public ProductBuilder withDiscountable(boolean discountable) {
        this.discountable = discountable;
        return this;
    }

    public ProductBuilder withDefaultPrice(double defaultPrice) {
        this.defaultPrice = defaultPrice;
        return this;
    }

    public ProductBuilder withDescriptionAttributes(Map<String, ?> descriptionAttributes) {
        this.descriptionAttributes = descriptionAttributes;
        return this;
    }

    public Product build() {
        return new Product(height, length, color, productType, leftInStock, discountable, defaultPrice, descriptionAttributes);
    }
}
