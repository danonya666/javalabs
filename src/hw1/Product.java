package hw1;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class Product {
    enum ProductType {
        SHIRT,
        MASK,
        TROUSERS,
        COMPUTER,
    }
    private final List<Product> similarProducts = new ArrayList<Product>();
    private final int height;
    private final int length;
    private final String color;
    private final ProductType productType;
    private final int leftInStock;
    private final boolean discountable;
    private final double defaultPrice;
    private final Map<String, ?> descriptionAttributes;

    public Product(int height, int length, String color, ProductType productType, int leftInStock, boolean discountable, double defaultPrice, Map<String, ?> descriptionAttributes) {
        this.height = height;
        this.length = length;
        this.color = color;
        this.productType = productType;
        this.leftInStock = leftInStock;
        this.discountable = discountable;
        this.defaultPrice = defaultPrice;
        this.descriptionAttributes = descriptionAttributes;
    }

    public Product() {
        this.height = -1;
        this.length = -1;
        this.color = null;
        this.productType = null;
        this.leftInStock = 0;
        this.discountable = false;
        this.defaultPrice = -1;
        this.descriptionAttributes = new HashMap<String, String>();
    }

    public List<Product> getSimilarProducts() {
        return similarProducts;
    }

    public int getHeight() {
        return height;
    }

    public int getLength() {
        return length;
    }

    public String getColor() {
        return color;
    }

    public ProductType getProductType() {
        return productType;
    }

    public int getLeftInStock() {
        return leftInStock;
    }

    public boolean isDiscountable() {
        return discountable;
    }

    public double getDefaultPrice() {
        return defaultPrice;
    }

    public Map<String, ?> getDescriptionAttributes() {
        return descriptionAttributes;
    }

    public static Product fromHashMap(HashMap<String, Object> builderMap) throws NoSuchFieldException, IllegalAccessException {
        final Field[] fields = Product.class.getDeclaredFields();
        Product p = new Product();
        for (Field field: fields) {
            field.setAccessible(true);
            Object fieldValue = builderMap.get(field.getName());
            if (fieldValue != null) {
                field.set(p, fieldValue);
            }
        }
        return p;
    }

    public HashMap<String, Object> toHashMap() throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        HashMap<String, Object> myObjectAsDict = new HashMap<String, Object>();
        Field[] allFields = Product.class.getDeclaredFields();
        for (Field field: allFields) {
            myObjectAsDict.put(field.getName(), field.get(this));
        }
        return myObjectAsDict;
    }

    private static void testToHashMap() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Product product = ProductBuilder.aProduct()
                .withProductType(ProductType.MASK)
                .withColor("blue")
                .withDefaultPrice(1000)
                .withDescriptionAttributes(new HashMap<String, Object>())
                .withDiscountable(true)
                .withHeight(10)
                .withLeftInStock(15)
                .withLength(15)
                .build();
        final HashMap<String, Object> productHashMap = product.toHashMap();
        System.out.println(productHashMap.get("productType") == ProductType.MASK? "PASSED": "FAILED");
        System.out.println(productHashMap.get("color") == "blue"? "PASSED": "FAILED");
    }

    private static void testFromHashMap() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, NoSuchFieldException {
        Product product = ProductBuilder.aProduct()
                .withProductType(ProductType.MASK)
                .withColor("blue")
                .withDefaultPrice(1000)
                .withDescriptionAttributes(new HashMap<String, Object>())
                .withDiscountable(true)
                .withHeight(10)
                .withLeftInStock(15)
                .withLength(15)
                .build();
        final HashMap<String, Object> productHashMap = product.toHashMap();
        final HashMap<String, Object> productHashMapWithNulls = new HashMap<String, Object>();
        final int TEST_PRODUCT_HEIGHT = 15;
        final String DEFAULT_PRODUCT_COLOR = null;
        productHashMapWithNulls.put("height", TEST_PRODUCT_HEIGHT);
        Product product1 = Product.fromHashMap(productHashMap);
        System.out.println(product.toHashMap().equals(product1.toHashMap()) ? "PASSED": "FAILED");
        Product product2 = Product.fromHashMap(productHashMapWithNulls);
        System.out.println(product2.getColor() == DEFAULT_PRODUCT_COLOR ? "PASSED": "FAILED");
        System.out.println(product2.getHeight() == TEST_PRODUCT_HEIGHT ? "PASSED": "FAILED");
    }
    public static void main(String[] args) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException, ClassNotFoundException {
        System.out.println("TEST CASES TO HASH MAP");
        Product.testToHashMap();
        System.out.println("TEST CASES FROM HASH MAP");
        Product.testFromHashMap();
    }
}
